package com.myrxpharmas.Common_TextView_Editext;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by VIVEK on 17-May-19.
 */

@SuppressLint("AppCompatCustomView")
public class TextViewRegular extends TextView {

   public TextViewRegular(Context context, AttributeSet attrs, int defStyle) {
            super(context, attrs, defStyle);
            init(context);
        }

  public TextViewRegular(Context context, AttributeSet attrs) {
            super(context, attrs);
            init(context);
        }

  public TextViewRegular(Context context) {
            super(context);
            init(context);
        }

  private void init(Context context) {
            Typeface tf = Typeface.createFromAsset(context.getAssets(),
                    "fonts/helveticaneue.ttf");
            setTypeface(tf);
        }

}
