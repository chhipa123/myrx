package com.myrxpharmas.ImageGettingCode;

import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;


import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.myrxpharmas.R;

import java.io.File;
import java.io.IOException;
import java.util.List;




public class ImageUploadActivty extends AppCompatActivity {

    private int REQUEST_CAMERA = 1, SELECT_FILE = 2;
    private final int RESULT_CROP = 400;
    Bitmap bitmapUserPic;
    String filePath="";
    File cameraImageFile;
    Uri uri_camera;
    ImageView imageview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        setContentView(R.layout.activity_image_upload_activty);
        imageview=(ImageView)findViewById(R.id.imageview) ;
        generatepop();
    }

    private void generatepop() {
        try {
            final CharSequence[] options = {"Take Photo", "Choose From Gallery","Cancel"};
            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(ImageUploadActivty.this);
            builder.setTitle("Select Option");
            builder.setCancelable(false);

            builder.setItems(options, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (options[item].equals("Take Photo")) {
                        dialog.dismiss();
                        addImage_Camera();
                    } else if (options[item].equals("Choose From Gallery")) {
                        dialog.dismiss();
                        addImage();
                    } else if (options[item].equals("Cancel")) {
                        dialog.dismiss();
                        Intent returnIntent = new Intent();
                        setResult(Activity.RESULT_CANCELED,returnIntent);
                        finish();
                        overridePendingTransition(0, 0);
                    }
                }
            });
            builder.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void addImage_Camera() {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.CAMERA
                )
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {

//                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                            startActivityForResult(intent, REQUEST_CAMERA);

                            Intent CamIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

//                            cameraImageFile = new File(Environment.getExternalStorageDirectory(),
//                                    "file" + String.valueOf(System.currentTimeMillis()) + ".jpg");
                            cameraImageFile=createNewFile("CROP_");
                            uri_camera = Uri.fromFile(cameraImageFile);

                            CamIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri_camera);
                            CamIntent.putExtra("return-data", true);
                            startActivityForResult(CamIntent, REQUEST_CAMERA);
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // permission is denied permenantly, navigate user to app settings
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .onSameThread()
                .check();


    }





    public void addImage() {
        Dexter.withActivity(ImageUploadActivty.this)
                .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(galleryIntent, SELECT_FILE);
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // permission is denied permenantly, navigate user to app settings
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .onSameThread()
                .check();

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (requestCode == SELECT_FILE && resultCode == RESULT_OK) {
                onSelectFromGalleryResult(data,1);
            } else if (requestCode == REQUEST_CAMERA && resultCode == RESULT_OK) {
//                Bitmap photo = (Bitmap) data.getExtras().get("data");
                onSelectFromGalleryResult(data,2);
            } else if (requestCode == RESULT_CROP && resultCode == RESULT_OK) {

                Bundle extras = data.getExtras();
                /*Extras null in version 8.0 andoid
                Samsung J6 else condition run in this case*/
                if (extras != null) {
                    if (data.getData() != null) {
                        Uri selectedImageUri = data.getData();
                        if (selectedImageUri != null) {
                            filePath = selectedImageUri.getPath();
                        }
                    }
                    bitmapUserPic = extras.getParcelable("data");
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("bitmap",bitmapUserPic);
                    returnIntent.putExtra("filepath", filePath);
                    if(cameraImageFile!=null)
                    {
                        if (cameraImageFile.exists()) {
                            /*Delete image*/
                            new RealPathUtil().deleteimageandfolder(ImageUploadActivty.this,cameraImageFile.getAbsolutePath().toString());
                        }
                    }
                    setResult(Activity.RESULT_OK,returnIntent);
                    this.finish();
                    overridePendingTransition(0, 0);

                }else{
                    Uri selectedImageUri = data.getData();
                    if (selectedImageUri != null) {
                        filePath = selectedImageUri.getPath();
                    }
                    bitmapUserPic = BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImageUri));
                   // bitmapUserPic = extras.getParcelable("data");
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("bitmap",bitmapUserPic);
                    returnIntent.putExtra("filepath",filePath);
                    if(cameraImageFile!=null)
                    {
                        if (cameraImageFile.exists()) {
                            /*Delete image*/
                            new RealPathUtil().deleteimageandfolder(ImageUploadActivty.this,cameraImageFile.getAbsolutePath().toString());
                        }
                    }
                    setResult(Activity.RESULT_OK,returnIntent);
                    this.finish();
                    overridePendingTransition(0, 0);
                }
            }else{
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED,returnIntent);
                finish();
                overridePendingTransition(0, 0);
            }
        }catch (Exception e)
        {
            e.printStackTrace();
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_CANCELED,returnIntent);
            finish();
            overridePendingTransition(0, 0);
        }
    }




    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data,int cases) {
        if(cases==1) {
            if (data != null) {

                try {
                    try {
                        if (Build.VERSION.SDK_INT < 11)
                            filePath = RealPathUtil.getRealPathFromURI_BelowAPI11(this, data.getData());
                            // SDK >= 11 && SDK < 19
                        else if (Build.VERSION.SDK_INT < 19)
                            filePath = RealPathUtil.getRealPathFromURI_API11to18(this, data.getData());
                            // Lolipop
                        else if(Build.VERSION.SDK_INT<=22)
                            filePath = RealPathUtil.getRealPathFromURI_API19(this, data.getData());
                        /*Kitkat to Pie*/
                        else
                            filePath = FileUtilAbove19.getPath(this, data.getData());
                    } catch (Exception e) {
                        Uri selectedImageUri = data.getData();
                        filePath = getRealPathFromURI(selectedImageUri);
                    }
                    if (filePath == null) {
                        Uri selectedImageUri = data.getData();
                        filePath = selectedImageUri.getPath();
                    }

                    if (filePath != null) {
                        performCrop(filePath);
                    }

                } catch (OutOfMemoryError E) {

                    System.gc();
                    Intent returnIntent = new Intent();
                    setResult(Activity.RESULT_CANCELED,returnIntent);
                    finish();
                    overridePendingTransition(0, 0);
                }
            }
        }else{
            if(cameraImageFile!=null) {
                performCrop(cameraImageFile.getAbsolutePath().toString());
            }else{
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED,returnIntent);
                finish();
                overridePendingTransition(0, 0);
            }
        }

    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }


    private void performCrop(String picUri) {
        Uri mCropImagedUri;
        Bitmap photo = BitmapFactory.decodeFile(picUri);
        try {
            //Start Crop Activity
            filePath = picUri;
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri
            File f = new File(picUri);
            Uri contentUri = Uri.fromFile(f);

            cropIntent.setDataAndType(contentUri, "image/*");
            // set crop properties
            cropIntent.putExtra("crop", "true");

            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 2);
            cropIntent.putExtra("aspectY", 2);

            // indicate output X and Y
            cropIntent.putExtra("outputX", 700);
            cropIntent.putExtra("outputY", 700);

            // retrieve data on return
            cropIntent.putExtra("scaleUpIfNeeded", true);
            cropIntent.putExtra("return-data", true);
            File fileN = createNewFile("CROP_");
            try {
                f.createNewFile();
            } catch (IOException ex) {
                Log.e("io", ex.getMessage());
            }

            mCropImagedUri = Uri.fromFile(fileN);
            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCropImagedUri);
            // start the custom_userfriends - we handle returning in onActivityResult
            startActivityForResult(cropIntent, RESULT_CROP);
        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            // display an error message
            String errorMessage = "your device doesn't support the crop action!";
            //Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            // toast.show();
        }
    }

    private File createNewFile(String prefix) {
        if (prefix == null || "".equalsIgnoreCase(prefix)) {
            prefix = "IMG_";
        }
        File newDirectory = new File(Environment.getExternalStorageDirectory() + "/crop/");
        if (!newDirectory.exists()) {
            if (newDirectory.mkdir()) {
                Log.d(this.getClass().getName(), newDirectory.getAbsolutePath() + " directory created");
            }
        }
        File file = new File(newDirectory, (prefix + System.currentTimeMillis() + ".jpg"));
        if (file.exists()) {
            //this wont be executed
            file.delete();
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return file;
    }


    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED,returnIntent);
        finish();
        overridePendingTransition(0, 0);
    }


    /*image_product get code on activtity result where we want to get the image and file url
    *
    *
     @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (IMAGE_RESPONCE == 200 && resultCode == RESULT_OK) {
              //  Bitmap bitmap = (Bitmap) data.getParcelableExtra("bitmap");
                String filepath=data.getStringExtra("filepath");
                Bitmap photo = BitmapFactory.decodeFile(filepath);
                File imgFile = new  File(filepath);
                if(imgFile.exists())
                {
                    imageview.setImageURI(Uri.fromFile(imgFile));
                }
                /*Delete image*/
//                new RealPathUtil().deleteimageandfolder(MainActivity.this,filepath);
//}
//        }catch (Exception e)
//                {
//                e.printStackTrace();
//                }
//                }
}
