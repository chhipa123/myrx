package com.myrxpharmas.Session_Manage;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.myrxpharmas.activity.Login_Activity;

/**
 * Created by ALOK TIWARI on 2/28/2017.
 */

public class UserPreferences {
    private static String SETTINGS_PREFS = "myrx_pharma";
    private static SharedPreferences sharedPreferences;
    private static Context mContext;
    private static SharedPreferences.Editor dataEditor;
    int PRIVATE_MODE = 0;

    public UserPreferences(Context _Context) {
        this.mContext = _Context;
        sharedPreferences = mContext.getSharedPreferences(SETTINGS_PREFS, PRIVATE_MODE);
        dataEditor = sharedPreferences.edit();
    }
    private static final String UID_MOBILE = "UDI_MOBILE";
    private static final String isLoginDone = "isLoginDone";



    public static void setUidMobile(String uidmobile) {
        dataEditor.putString(UserPreferences.UID_MOBILE, uidmobile);
        dataEditor.commit();
    }


    public static String getUidMobile() {
        return sharedPreferences.getString(UserPreferences.UID_MOBILE, "");
    }


    public static void setVerification(boolean isVerified) {
        dataEditor.putBoolean(UserPreferences.isLoginDone, isVerified);
        dataEditor.commit();
    }

    public static boolean isVerified() {
        try{
            return sharedPreferences.getBoolean(isLoginDone, false);
        }catch (Exception e){
            return false;
        }

    }


    public static void logout() {
        dataEditor.clear();
        dataEditor.commit();
        Intent newIntent = new Intent(mContext, Login_Activity.class);
        newIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(newIntent);
    }
}
