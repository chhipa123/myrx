package com.myrxpharmas.Session_Manage;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by VIVEK on 16-May-19.
 */

public class UserPrefence_Token {

    private static String SETTINGS_PREFS = "myrx_pharma_token";
    private static SharedPreferences sharedPreferences_token;
    private static Context mContext_token;
    private static SharedPreferences.Editor dataEditor_token;
    int PRIVATE_MODE = 0;



    public UserPrefence_Token(Context _Context) {
        this.mContext_token = _Context;
        sharedPreferences_token = mContext_token.getSharedPreferences(SETTINGS_PREFS, PRIVATE_MODE);
        dataEditor_token = sharedPreferences_token.edit();
    }

    private static final String FCM_TOKEN = "FCM_TOKEN";

    public static void setFcmToken(String deviceId) {
        dataEditor_token.putString(FCM_TOKEN, deviceId);
        dataEditor_token.commit();
    }

    public static String getFcmToken() {
        return sharedPreferences_token.getString(FCM_TOKEN, "");
    }
}
