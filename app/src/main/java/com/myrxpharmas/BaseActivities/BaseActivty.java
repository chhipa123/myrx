package com.myrxpharmas.BaseActivities;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.myrxpharmas.R;

public class BaseActivty extends AppCompatActivity implements View.OnClickListener{

    protected DrawerLayout mDrawerLayout;
    FrameLayout frame_layout;
    private ActionBarDrawerToggle mDrawerToggle;
    NavigationView nav_view;
    ImageView btn_home,btn_bag,btn_order,btn_learn,btn_help,menuicon;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_activty);

    }

    public void setActionBars( String headerTitle) {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        nav_view=(NavigationView)findViewById(R.id.nav_view);
        frame_layout = (FrameLayout) findViewById(R.id.frame_layout);

        btn_home=(ImageView)findViewById(R.id.btn_home);
        btn_bag=(ImageView)findViewById(R.id.btn_bag);
        btn_order=(ImageView)findViewById(R.id.btn_order);
        btn_learn=(ImageView)findViewById(R.id.btn_learn);
        btn_help=(ImageView)findViewById(R.id.btn_help);
        menuicon=(ImageView)findViewById(R.id.menuicon);

        btn_home.setOnClickListener(this);
        btn_bag.setOnClickListener(this);
        btn_order.setOnClickListener(this);
        btn_learn.setOnClickListener(this);
        btn_help.setOnClickListener(this);
        menuicon.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btn_home:
                clearallimagecolor(btn_home,R.drawable.home);
                break;
            case R.id.btn_bag:
                clearallimagecolor(btn_bag, R.drawable.cart_selected);
                break;
            case R.id.btn_order:
                clearallimagecolor(btn_order, R.drawable.order_selected);
                break;
            case R.id.btn_learn:
                clearallimagecolor(btn_learn, R.drawable.learn_selected);
                break;
            case R.id.btn_help:
                clearallimagecolor(btn_help, R.drawable.help_selected);
                break;

        }
    }

    public void clearallimagecolor(final ImageView imageView, int imageiname){
        btn_home.setImageResource(R.drawable.home_deselected);
        btn_bag.setImageResource(R.drawable.cart);
        btn_order.setImageResource(R.drawable.orderd);
        btn_learn.setImageResource(R.drawable.learnd);
        btn_help.setImageResource(R.drawable.help);

        /*set slected image color*/

       imageView.setImageResource(imageiname);
    }
    public int adjustAlpha(int color, float factor) {
        int alpha = Math.round(Color.alpha(color) * factor);
        int red = Color.red(color);
        int green = Color.green(color);
        int blue = Color.blue(color);
        return Color.argb(alpha, red, green, blue);
    }
}
