package com.myrxpharmas.activity;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.myrxpharmas.BaseActivities.BaseActivty;
import com.myrxpharmas.R;
import com.myrxpharmas.adapter.PhotosAdapter;
import com.myrxpharmas.adapter.ProductList_Adapter;

import java.util.ArrayList;
import java.util.List;


public class HomeScreen extends BaseActivty {

    int[] mResources = {
            R.drawable.banner_image,
            R.drawable.banner_image,
            R.drawable.banner_image,
            R.drawable.banner_image,
            R.drawable.banner_image,
            R.drawable.banner_image
    };
    RecyclerView recylerview;
    private List<String> movieList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initUi();

    }

    private void initUi() {
        setContentView(R.layout.drawerbase00);
        ViewStub stub = (ViewStub) findViewById(R.id.includeBase2);
        stub.setLayoutResource(R.layout.activity_home_screen);
        View inflated = stub.inflate();
        inits(inflated);
        setActionBars("Products");
    }

    private void inits(View inflated) {
        ViewPager pager = (ViewPager) inflated.findViewById(R.id.photos_viewpager);
        PagerAdapter adapter = new PhotosAdapter(this,mResources);
        pager.setAdapter(adapter);

        TabLayout tabLayout = (TabLayout) inflated.findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(pager, true);



        movieList.add("1");
        movieList.add("2");
        movieList.add("3");
        movieList.add("4");

        recylerview=(RecyclerView)inflated.findViewById(R.id.recylerview);
        ProductList_Adapter mAdapter = new ProductList_Adapter(this,movieList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recylerview.setLayoutManager(mLayoutManager);
        recylerview.setItemAnimator(new DefaultItemAnimator());
        recylerview.setAdapter(mAdapter);
    }



}
