package com.myrxpharmas.activity;

import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.myrxpharmas.Constants.Constant;
import com.myrxpharmas.R;
import com.myrxpharmas.ResopnceModel.RegistrationModel;
import com.myrxpharmas.Utility.ProgressBarCustom;
import com.myrxpharmas.Utility.Utils;
import com.myrxpharmas.api.RestClient;

import org.json.JSONObject;

import retrofit.RetrofitError;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;
import retrofit.Callback;
import retrofit.client.Response;


public class Registration_Activity extends AppCompatActivity implements View.OnClickListener{

    TextView txt_signin;
    EditText et_email,et_password,et_repeat_pass;
    TextView btn_sign_up;
    ProgressBarCustom progressBarCustom;
    TextInputLayout input_layout_email,input_layout_password,input_layout_repeat_pass;
    ImageView img_email,img_password,img_repeat_password;
    TextView mandatory_text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_);
        inits();

    }

    private void inits() {

        txt_signin=(TextView)findViewById(R.id.txt_signin);

        et_email=(EditText)findViewById(R.id.et_email);
        et_password=(EditText)findViewById(R.id.et_password);
        et_repeat_pass=(EditText)findViewById(R.id.et_repeat_pass);

        btn_sign_up=(TextView) findViewById(R.id.btn_sign_up);

        input_layout_email=(TextInputLayout)findViewById(R.id.input_layout_email);
        input_layout_password=(TextInputLayout)findViewById(R.id.input_layout_password);
        input_layout_repeat_pass=(TextInputLayout)findViewById(R.id.input_layout_repeat_pass);

        img_password=(ImageView)findViewById(R.id.img_password);
        img_email=(ImageView)findViewById(R.id.img_email);
        img_repeat_password=(ImageView)findViewById(R.id.img_repeat_password);
        mandatory_text=(TextView)findViewById(R.id.mandatory_text);

        btn_sign_up.setOnClickListener(this);
        txt_signin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.txt_signin:
                overridePendingTransition(0, 0);
                finish();
                break;
            case R.id.btn_sign_up :
                validation();
                break;

        }
    }

    private void validation() {
        String getemail=et_email.getText().toString();
        if(et_email.getText().length()==0)
        {
           // new Utils(Registration_Activity.this).showCommonToast(getResources().getString(R.string.email_field_blank));
            input_layout_email.requestFocus();
            input_layout_email.setError(getResources().getString(R.string.email_field_blank));
            input_layout_email.setHintTextAppearance(R.style.Inactive);
            img_email.setColorFilter(getResources().getColor(R.color.error_color));
            et_email.setTextColor(getResources().getColor(R.color.error_color));
            mandatory_text.setVisibility(View.VISIBLE);

        }
        else if(new Utils(Registration_Activity.this).isEmailValid(getemail)==false)
        {
            //new Utils(Registration_Activity.this).showCommonToast(getResources().getString(R.string.invalid_email));
            input_layout_email.setError(getResources().getString(R.string.invalid_email));
            input_layout_email.setHintTextAppearance(R.style.Inactive);
            img_email.setColorFilter(getResources().getColor(R.color.error_color));
            et_email.setTextColor(getResources().getColor(R.color.error_color));
            mandatory_text.setVisibility(View.VISIBLE);
        }
        else if(et_password.getText().length()==0)
        {
            img_email.setColorFilter(getResources().getColor(R.color.backcolor_edit));
            input_layout_email.setError(null);
            input_layout_email.setHintTextAppearance(R.style.Active);
            et_email.setTextColor(getResources().getColor(R.color.white));

            et_password.requestFocus();
            input_layout_password.setHintTextAppearance(R.style.Inactive);
            img_password.setColorFilter(getResources().getColor(R.color.error_color));
            input_layout_password.setError(getResources().getString(R.string.pass_field_blank));
            et_password.setHintTextColor(getResources().getColor(R.color.error_color));
            et_password.setTextColor(getResources().getColor(R.color.error_color));

            mandatory_text.setVisibility(View.GONE);
        }
        else if(et_password.getText().length()<8)
        {
            img_email.setColorFilter(getResources().getColor(R.color.backcolor_edit));
            input_layout_email.setError(null);
            input_layout_email.setHintTextAppearance(R.style.Active);
            et_email.setTextColor(getResources().getColor(R.color.white));

            et_password.requestFocus();
            input_layout_password.setHintTextAppearance(R.style.Inactive);
            img_password.setColorFilter(getResources().getColor(R.color.error_color));
            input_layout_password.setError(getResources().getString(R.string.password_length));
            et_password.setHintTextColor(getResources().getColor(R.color.error_color));
            et_password.setTextColor(getResources().getColor(R.color.error_color));
            mandatory_text.setVisibility(View.GONE);
        }
        else if(et_repeat_pass.getText().length()==0)
        {
            img_email.setColorFilter(getResources().getColor(R.color.backcolor_edit));
            input_layout_email.setError(null);
            input_layout_email.setHintTextAppearance(R.style.Active);
            et_email.setTextColor(getResources().getColor(R.color.white));

            img_password.setColorFilter(getResources().getColor(R.color.backcolor_edit));
            input_layout_password.setError(null);
            input_layout_password.setHintTextAppearance(R.style.Active);
            et_password.setTextColor(getResources().getColor(R.color.white));
            mandatory_text.setVisibility(View.GONE);
            //new Utils(Registration_Activity.this).showCommonToast(getResources().getString(R.string.repeat_pass_field_blank));

            input_layout_repeat_pass.setHintTextAppearance(R.style.Inactive);
            img_repeat_password.setColorFilter(getResources().getColor(R.color.error_color));
            input_layout_repeat_pass.setError(getResources().getString(R.string.repeat_pass_field_blank));
            et_repeat_pass.setHintTextColor(getResources().getColor(R.color.error_color));
            et_repeat_pass.setTextColor(getResources().getColor(R.color.error_color));
        }
        else if(!et_password.getText().toString().equals(et_repeat_pass.getText().toString()))
        {
            img_email.setColorFilter(getResources().getColor(R.color.backcolor_edit));
            input_layout_email.setError(null);
            input_layout_email.setHintTextAppearance(R.style.Active);
            et_email.setTextColor(getResources().getColor(R.color.white));

            img_password.setColorFilter(getResources().getColor(R.color.backcolor_edit));
            input_layout_password.setError(null);
            input_layout_password.setHintTextAppearance(R.style.Active);
            et_password.setTextColor(getResources().getColor(R.color.white));


            input_layout_repeat_pass.setHintTextAppearance(R.style.Inactive);
            img_repeat_password.setColorFilter(getResources().getColor(R.color.error_color));
            input_layout_repeat_pass.setError(getResources().getString(R.string.password_not_same));
            et_repeat_pass.setHintTextColor(getResources().getColor(R.color.error_color));
            et_repeat_pass.setTextColor(getResources().getColor(R.color.error_color));

            mandatory_text.setVisibility(View.GONE);
        }else{
            img_email.setColorFilter(getResources().getColor(R.color.backcolor_edit));
            input_layout_email.setError(null);
            input_layout_email.setHintTextAppearance(R.style.Active);
            et_email.setTextColor(getResources().getColor(R.color.white));

            img_password.setColorFilter(getResources().getColor(R.color.backcolor_edit));
            input_layout_password.setError(null);
            input_layout_password.setHintTextAppearance(R.style.Active);
            et_password.setTextColor(getResources().getColor(R.color.white));

            img_repeat_password.setColorFilter(getResources().getColor(R.color.backcolor_edit));
            input_layout_repeat_pass.setError(null);
            input_layout_repeat_pass.setHintTextAppearance(R.style.Active);
            et_repeat_pass.setTextColor(getResources().getColor(R.color.white));
            mandatory_text.setVisibility(View.GONE);

            callapi();
        }

    }

    private void callapi() {

            progressBarCustom=new ProgressBarCustom(Registration_Activity.this);
            progressBarCustom.showProgress();
            try {
                JSONObject jsonObject3 = new JSONObject();
                jsonObject3.put("email", et_email.getText().toString());
                jsonObject3.put("password",  et_password.getText().toString());
                TypedInput in3 = new TypedByteArray("application/json", jsonObject3.toString().getBytes("UTF-8"));

                RestClient.getInstance(this).get().regitsration( in3, new Callback<RegistrationModel>() {

                    @Override
                    public void success(final RegistrationModel res, Response response) {
                        progressBarCustom.cancelProgress();
                        if(res.getStatusCode()== Constant.RESPONCE_SUCCESS)
                        {
                            new Utils(Registration_Activity.this).showCommonToast(res.getResponseMessage());
                            finish();

                        }


                    }

                    @Override
                    public void failure(final RetrofitError error) {
                        progressBarCustom.cancelProgress();
                        new Utils(Registration_Activity.this).showCommonToast(error.getMessage());

                    }
                });


            }catch (Exception e)
            {
                progressBarCustom.cancelProgress();
                //progress.cancelProgress();
                new Utils(Registration_Activity.this).showCommonToast(e.getMessage());
            }
    }
}
