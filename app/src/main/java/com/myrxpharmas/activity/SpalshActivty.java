package com.myrxpharmas.activity;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.myrxpharmas.R;
import com.myrxpharmas.Session_Manage.UserPreferences;


public class SpalshActivty extends AppCompatActivity {

    private Thread mThread;
    private boolean isFinish = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        new UserPreferences(SpalshActivty.this);
        mThread = new Thread(mRunnable);
        mThread.start();
    }

    private Runnable mRunnable = new Runnable() {

        @Override
        public void run() {
            SystemClock.sleep(3000);
            mHandler.sendEmptyMessage(0);
        }
    };

    private Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            if (msg.what == 0 && (!isFinish)) {
                stop();
            }
            super.handleMessage(msg);
        }

    };

    @Override
    protected void onDestroy() {
        try {
            mThread.interrupt();
            mThread = null;
        } catch (Exception e) {
        }
        super.onDestroy();
    }

    private void stop() {
        isFinish = true;

        Intent intent = new Intent(SpalshActivty.this, Login_Activity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(0, 0);
        finish();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        isFinish = true;
        try {
            mThread.interrupt();
            mThread = null;
        } catch (Exception e) {
        }
        finish();
    }

    @Override
    public void finish() {
        super.finish();

    }

}
