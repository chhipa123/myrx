package com.myrxpharmas.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;


import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.myrxpharmas.R;
import com.myrxpharmas.ResopnceModel.RegistrationModel;
import com.myrxpharmas.ResopnceModel.VerifyEmailModel;
import com.myrxpharmas.Session_Manage.UserPreferences;
import com.myrxpharmas.Utility.NetworkDetector;
import com.myrxpharmas.Utility.ProgressBarCustom;
import com.myrxpharmas.Utility.Utils;
import com.myrxpharmas.api.RestClient;

import org.json.JSONObject;

import java.util.Arrays;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static com.facebook.FacebookSdk.getApplicationContext;


public class Login_Activity extends AppCompatActivity implements View.OnClickListener{


    TextView txt_signup,txt_sign;
    ImageView btn_google,btn_facebook;
    GoogleSignInClient mGoogleSignInClient;
    GoogleApiClient mGoogleApiClient;
    private FirebaseAuth mAuth;
    private CallbackManager callbackManager;
    public final int GOOGLE_CALLABACK=250;
    EditText et_email,et_password;
    TextInputLayout input_layout_email,input_layout_password;
    TextView mandatory_text;
    ImageView img_email,img_password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(getApplicationContext());
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        inits();
        googlesigninsetup();
    }

    private void inits() {
        callbackManager = CallbackManager.Factory.create();
        txt_signup=(TextView)findViewById(R.id.txt_signup);
        txt_sign=(TextView)findViewById(R.id.txt_sign);
        btn_google=(ImageView)findViewById(R.id.btn_google);
        btn_facebook=(ImageView)findViewById(R.id.btn_facebook);

        et_email=(EditText)findViewById(R.id.et_email);
        et_password=(EditText)findViewById(R.id.et_password);

        txt_sign.setOnClickListener(this);
        txt_signup.setOnClickListener(this);
        btn_facebook.setOnClickListener(this);
        btn_google.setOnClickListener(this);

        input_layout_email=(TextInputLayout)findViewById(R.id.input_layout_email);
        input_layout_password=(TextInputLayout)findViewById(R.id.input_layout_password);



        img_email=(ImageView)findViewById(R.id.img_email);
        img_password=(ImageView)findViewById(R.id.img_password);

        mandatory_text=(TextView)findViewById(R.id.mandatory_text);

        checkPermissionUid();

    }

    private void googlesigninsetup() {
        try {
            mAuth = FirebaseAuth.getInstance();

            //Then we need a GoogleSignInOptions object
            //And we need to build it as below
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestIdToken(getString(R.string.default_web_client_id))
                    .requestEmail()
                    .build();

            //Then we will get the GoogleSignInClient object from GoogleSignIn class
            mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .enableAutoManage(this, new GoogleApiClient.OnConnectionFailedListener() {
                        @Override
                        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        Log.e("=======",connectionResult.getErrorMessage());
                        }


                    } /* OnConnectionFailedListener */)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.txt_signup:
                et_password.setText("");
                et_email.setText("");
                et_password.clearFocus();
                et_email.clearFocus();

                resetchecks();

                Intent intent=new Intent(Login_Activity.this,Registration_Activity.class);
                startActivity(intent);
                overridePendingTransition(0, 0);
                break;
            case R.id.txt_sign:
                validation();
                break;
            case R.id.btn_google:
                if(new NetworkDetector(Login_Activity.this).isConnectingToInternet())
                {
                    signIn();
                }else{
                    new Utils(getApplicationContext()).showCommonToast(getResources().getString(R.string.internet_connection));
                }
                    break;
            case R.id.btn_facebook:
                if(new NetworkDetector(Login_Activity.this).isConnectingToInternet())
                {
                    launchFacebook();
                }else{
                    new Utils(getApplicationContext()).showCommonToast(getResources().getString(R.string.internet_connection));
                }
                break;

        }
    }

    private void validation() {
        String getemail=et_email.getText().toString();
        if(et_email.getText().length()==0)
        {
            input_layout_email.requestFocus();
            input_layout_email.setError(getResources().getString(R.string.email_field_blank));
            input_layout_email.setHintTextAppearance(R.style.Inactive);
            img_email.setColorFilter(getResources().getColor(R.color.error_color));
            et_email.setTextColor(getResources().getColor(R.color.error_color));
            mandatory_text.setVisibility(View.VISIBLE);
        }
        else if(new Utils(getApplicationContext()).isEmailValid(getemail)==false)
        {
            
            input_layout_email.setError(getResources().getString(R.string.invalid_email));
            input_layout_email.setHintTextAppearance(R.style.Inactive);
            img_email.setColorFilter(getResources().getColor(R.color.error_color));
            et_email.setTextColor(getResources().getColor(R.color.error_color));
            mandatory_text.setVisibility(View.VISIBLE);

        }
        else if(et_password.getText().length()==0)
        {
            img_email.setColorFilter(getResources().getColor(R.color.backcolor_edit));
            input_layout_email.setError(null);
            input_layout_email.setHintTextAppearance(R.style.Active);
            et_email.setTextColor(getResources().getColor(R.color.white));

            et_password.requestFocus();
            input_layout_password.setHintTextAppearance(R.style.Inactive);
            img_password.setColorFilter(getResources().getColor(R.color.error_color));
            input_layout_password.setError(getResources().getString(R.string.pass_field_blank));
            et_password.setHintTextColor(getResources().getColor(R.color.error_color));
            et_password.setTextColor(getResources().getColor(R.color.error_color));
            mandatory_text.setVisibility(View.GONE);

        }
        else if(et_password.getText().length()<8)
        {


            img_email.setColorFilter(getResources().getColor(R.color.backcolor_edit));
            input_layout_email.setError(null);
            input_layout_email.setHintTextAppearance(R.style.Active);
            et_email.setTextColor(getResources().getColor(R.color.white));

            et_password.requestFocus();
            input_layout_password.setHintTextAppearance(R.style.Inactive);
            img_password.setColorFilter(getResources().getColor(R.color.error_color));
            input_layout_password.setError(getResources().getString(R.string.password_length));
            et_password.setHintTextColor(getResources().getColor(R.color.error_color));
            et_password.setTextColor(getResources().getColor(R.color.error_color));

            mandatory_text.setVisibility(View.GONE);

        }
        else{

            resetchecks();

            Intent intent1=new Intent(Login_Activity.this,HomeScreen.class);
            startActivity(intent1);
            overridePendingTransition(0, 0);
            finish();
        }

    }

    private void resetchecks() {
        img_email.setColorFilter(getResources().getColor(R.color.backcolor_edit));
        input_layout_email.setError(null);
        input_layout_email.setHintTextAppearance(R.style.Active);
        et_email.setTextColor(getResources().getColor(R.color.white));

        img_password.setColorFilter(getResources().getColor(R.color.backcolor_edit));
        input_layout_password.setError(null);
        input_layout_password.setHintTextAppearance(R.style.Active);
        et_password.setTextColor(getResources().getColor(R.color.white));
        mandatory_text.setVisibility(View.GONE);
    }

    private void signIn() {
        if(mAuth!=null)
        {
            mAuth.signOut();
        }

        Intent signInIntent = mGoogleSignInClient.getSignInIntent();

        //starting the activity for result
        startActivityForResult(signInIntent, GOOGLE_CALLABACK);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == GOOGLE_CALLABACK) {

            //Getting the GoogleSignIn Task
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                //Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                Log.e("======account",account.getEmail());
                //authenticating with firebase
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                e.printStackTrace();
            }
        }

        /*Facebook Callbak*/
       // callbackManager.onActivityResult(requestCode, resultCode, data);
    }


    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d("==fireabse", "firebaseAuthWithGoogle:" + acct.getId());

        //getting the auth credential
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);

        //Now using firebase we are signing in the user here
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(" sucesfull====", "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();

                            Toast.makeText(Login_Activity.this, "User Signed In", Toast.LENGTH_SHORT).show();
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("Failed====", "signInWithCredential:failure", task.getException());
                            Toast.makeText(Login_Activity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();

                        }

                        // ...
                    }
                });
    }


    private void launchFacebook() {
        LoginManager.getInstance().logInWithReadPermissions(Login_Activity.this, Arrays.asList("public_profile", "user_about_me", "user_relationships", "user_birthday", "user_location", "email"));
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                Profile profile = Profile.getCurrentProfile();
                Log.e("OnGraph", "------------------------******************");
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                // Application code

                                try {

                                    String email = "", imageURL = "", facebookID = "", name = "", gender = "", birthday = "";
                                    JSONObject json = response.getJSONObject();

                                    if (json.has("user_birthday")) {

                                    }

                                    if (json.has("email")) {
                                        email = json.getString("email");
                                        if(email!=null&&!email.equalsIgnoreCase(""))
                                        {
                                           // UserPreference.setEmail(email);
                                        }
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,link,gender,birthday,email");
                request.setParameters(parameters);
                request.executeAsync();

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });

    }

    @Override protected void onDestroy() {
        super.onDestroy();
        try {
            LoginManager.getInstance().logOut();
            AccessToken.setCurrentAccessToken(null);

        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void checkPermissionUid() {
        try {
            Dexter.withActivity(Login_Activity.this)
                    .withPermission(Manifest.permission.READ_PHONE_STATE)
                    .withListener(new PermissionListener() {
                        @Override
                        public void onPermissionGranted(PermissionGrantedResponse response) {
                            TelephonyManager TelephonyMgr = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
                            @SuppressLint("MissingPermission")
                            String m_deviceId = TelephonyMgr.getDeviceId();
                            new UserPreferences(Login_Activity.this).setUidMobile(m_deviceId);

                        }

                        @Override
                        public void onPermissionDenied(PermissionDeniedResponse response) {

                        }

                        @Override
                        public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {

                        }
                    }).check();
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        try {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                Uri data = getIntent().getData();
                //get schma
                if (data != null) {
                    String scheme = data.getScheme(); // "http"
                    //get server name
                    String host = data.getHost(); // Ipaddress or domain name

                    String full_url = data.toString();
                    // String result = full_url.substring(full_url.lastIndexOf('/') + 1).trim();
                    Toast.makeText(this, full_url + "", Toast.LENGTH_SHORT).show();
                    callApi();
                }

            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void callApi() {

       final ProgressBarCustom progressBarCustom=new ProgressBarCustom(Login_Activity.this);
        progressBarCustom.showProgress();
        RestClient.getInstance(this).get().verifyemail("", new Callback<VerifyEmailModel>() {

            @Override
            public void success(final VerifyEmailModel res, Response response) {
                progressBarCustom.cancelProgress();
//                if(res.getStatusCode()== Constant.RESPONCE_SUCCESS)
//                {
//                    new Utils(Login_Activity.this).showCommonToast(res.getResponseMessage());
//                }


            }

            @Override
            public void failure(final RetrofitError error) {
                progressBarCustom.cancelProgress();
                new Utils(Login_Activity.this).showCommonToast(error.getMessage());

            }
        });
    }
}
