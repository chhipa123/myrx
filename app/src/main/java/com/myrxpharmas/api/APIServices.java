package com.myrxpharmas.api;

import com.myrxpharmas.ResopnceModel.LoginModel;
import com.myrxpharmas.ResopnceModel.RegistrationModel;
import com.myrxpharmas.ResopnceModel.VerifyEmailModel;

import retrofit.http.POST;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.mime.TypedInput;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Headers;
public interface APIServices {


    @POST("/api/v1/public/patient")
    void regitsration(@Body TypedInput body, Callback<RegistrationModel> blogOutput);

    //http://172.16.0.210:8791/userauth/oauth/token?grant_type=password&roleId=3&username=ankur.verma@lmsin.com&password=abcdefgh

    @POST("/userauth/oauth/token")
    void loginuser(@Query("grant_type") String grant_type, @Query("roleId") String roleId,
                   @Query("username") String username, @Query("password") String password
            , Callback<LoginModel> loginModelCallback);


    @POST("/api/v1/public/patient/verifyemail/{token}")
    void verifyemail(@Path("token") String token, Callback<VerifyEmailModel> verifyEmailModelCallback);



}

