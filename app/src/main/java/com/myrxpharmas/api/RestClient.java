package com.myrxpharmas.api;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.myrxpharmas.R;
import com.squareup.okhttp.OkHttpClient;


import java.util.concurrent.TimeUnit;


import com.myrxpharmas.Utility.NetworkDetector;
import com.myrxpharmas.Utility.ProgressBarCustom;
import retrofit.ErrorHandler;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;


@SuppressWarnings("deprecation")
public class RestClient extends AppCompatActivity {

    private static RestClient sInstance;

    private static APIServices REST_CLIENT;
    private Context mContext;
    private static String ROOT = "http://172.16.0.210:8791";


    private RestClient(Context context) {
        mContext = context.getApplicationContext();
            init();
    }

    public static RestClient getInstance(Context context) {
            if (sInstance == null) {
                sInstance = new RestClient(context);
            }
        return sInstance;
    }

    private void init() {
//
        final OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(30, TimeUnit.SECONDS);
        okHttpClient.setConnectTimeout(10, TimeUnit.SECONDS);

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(ROOT)
                .setClient(new OkClient(okHttpClient))
                .setErrorHandler(new CustomErrorHandler(mContext))
                .build();


        REST_CLIENT = restAdapter.create(APIServices.class);
    }

    public static APIServices get() {
        return REST_CLIENT;
    }

    private  class CustomErrorHandler implements ErrorHandler{
        private final Context ctx;

        public CustomErrorHandler(Context ctx) {
            this.ctx = ctx;
        }

        @Override
        public Throwable handleError(RetrofitError cause) {
            String errorDescription = "sd";
            try {
                Response r = cause.getResponse();
                if(new NetworkDetector(ctx).isConnectingToInternet()){
                Log.e("=====cstar", cause.getMessage().toString() + "" + " " + r.getStatus());
                Log.e("=====cstar", cause.getCause().toString() + "" + " " + r.getStatus());
                    if (r != null && r.getStatus() == 401) {
                        errorDescription = "Unauthorized";
                    } else {
                        errorDescription = "Unable to communicate to server. Please try again.";
                        if (r.getStatus() == 200) {
                            errorDescription = "Rejected";
                        }
                    }
                }else{
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            new ProgressBarCustom(ctx).cancelProgress();
                            Toast.makeText(ctx, ctx.getResources().getString(R.string.internet_connection), Toast.LENGTH_SHORT).show();
                        }
                    });

                }

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return new Exception(errorDescription);
        }
    }


}
