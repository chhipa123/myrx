package com.myrxpharmas.ResopnceModel;

/**
 * Created by VIVEK on 13-May-19.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RegistrationModel {
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("responseMessage")
    @Expose
    private String responseMessage;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }


}
