package com.myrxpharmas.adapter;

import java.util.List;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.myrxpharmas.R;

/**
 * Created by VIVEK on 14-May-19.
 */
public class ProductList_Adapter extends RecyclerView.Adapter<ProductList_Adapter.MyViewHolder> {

    private List<String> moviesList;
    Context mcontext;
    public ProductList_Adapter(Context context, List<String> moviesList) {
        this.moviesList = moviesList;
        this.mcontext=context;
    }
    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView image_product;

        public MyViewHolder(View view) {
            super(view);
            //image_product=(ImageView)view.findViewById(R.id.image_product);
        }
    }


}